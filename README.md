# Red Moon Rising #

Red Moon Rising GSG, license depository.

## Unlicensed ##

Fix as soon as possible.

* [ART]		Portraits: /resources/art/portraits/
* [TEXT]	Name lists: /resources/localization/

## Licensed ##

### Free License ###

#### SIL Open Font License (OFL) ####

* [FONT]	Avara: /resources/fonts/avara/
* [FONT]	Cat Childs: /resources/fonts/cat_childs/

#### Creative Commons 3.0 BY ####

* [ART]	Icons: /resources/art/icons/
** Abstract 080: Viscious Speed