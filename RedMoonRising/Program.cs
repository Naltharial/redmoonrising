﻿using System;

using SFML.Graphics;
using SFML.Window;

namespace RedMoonRising
{
    static class RedMoonRising
    {
        private static source.Game game;

        static void Main()
        {
            var x = uint.Parse(source.settings.Config.ReadSetting("Window_Width"));
            var y = uint.Parse(source.settings.Config.ReadSetting("Window_Height"));
            var dm = uint.Parse(source.settings.Config.ReadSetting("Display_Mode"));
            var aa = uint.Parse(source.settings.Config.ReadSetting("Antialiasing"));

            var fps = uint.Parse(source.settings.Config.ReadSetting("FPS_Limit"));
            var cps = uint.Parse(source.settings.Config.ReadSetting("CPS_Limit"));

            var settings = new ContextSettings();
            settings.AntialiasingLevel = aa;

            RenderWindow window;
            switch (dm)
            {
                case 1:
                    window = new RenderWindow(new VideoMode(x, y), "Red Moon Rising", Styles.Close | Styles.Titlebar, settings);
                    break;
                case 2:
                    window = new RenderWindow(new VideoMode(VideoMode.DesktopMode.Width+1, VideoMode.DesktopMode.Height+1), "Red Moon Rising", Styles.None, settings);
                    break;
                default:
                    window = new RenderWindow(new VideoMode(x, y), "Red Moon Rising", Styles.Fullscreen, settings);
                    break;
            }
            
            window.Closed += new EventHandler(OnClosed);
            window.KeyPressed += new EventHandler<KeyEventArgs>(OnEvent);
            window.MouseMoved += new EventHandler<MouseMoveEventArgs>(OnEvent);
            window.MouseButtonPressed += new EventHandler<MouseButtonEventArgs>(OnMousePressEvent);
            window.MouseButtonReleased += new EventHandler<MouseButtonEventArgs>(OnMouseReleaseEvent);
            window.MouseWheelMoved += new EventHandler<MouseWheelEventArgs>(OnEvent);

            game = new source.Game(fps, cps, window);
            game.GameLoop();
        }
        
        static void OnClosed(object sender, EventArgs e)
        {
            RenderWindow window = (RenderWindow)sender;
            window.Close();
        }
        static void OnMousePressEvent(object sender, MouseButtonEventArgs e)
        {
            OnEvent(sender, MouseButtonPressedEventArgs.FromBase(e));
        }
        static void OnMouseReleaseEvent(object sender, MouseButtonEventArgs e)
        {
            OnEvent(sender, MouseButtonReleasedEventArgs.FromBase(e));
        }
        static void OnEvent(object sender, EventArgs e)
        {
            RenderWindow window = (RenderWindow)sender;

            game.HandleInput(e);
        }

        /* Manually disambiguate between the two */
        public class MouseButtonPressedEventArgs : MouseButtonEventArgs
        {
            public static MouseButtonPressedEventArgs FromBase(MouseButtonEventArgs mbea)
            {
                var mbe = new MouseButtonEvent();
                mbe.Button = mbea.Button;
                mbe.X = mbea.X;
                mbe.Y = mbea.Y;

                return new MouseButtonPressedEventArgs(mbe);
            }

            public MouseButtonPressedEventArgs(MouseButtonEvent e)
                :base(e)
            {

            }
        }
        public class MouseButtonReleasedEventArgs : MouseButtonEventArgs
        {
            public static MouseButtonReleasedEventArgs FromBase(MouseButtonEventArgs mbea)
            {
                var mbe = new MouseButtonEvent();
                mbe.Button = mbea.Button;
                mbe.X = mbea.X;
                mbe.Y = mbea.Y;

                return new MouseButtonReleasedEventArgs(mbe);
            }

            public MouseButtonReleasedEventArgs(MouseButtonEvent e)
                :base(e)
            {

            }
        }
    }
}
