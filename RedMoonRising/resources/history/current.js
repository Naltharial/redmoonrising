﻿history = {
    'factions': {
        'a4d7bb25-cad2-48b9-93ec-fbfe660a1331': {
            'name': "The Kingdom",
        },
        'ef972728-c7ca-4c89-b459-0d25f6a6cb65': {
            'name': "The Empire",
        },
        '6ad7fd7c-e5c5-48de-a3d3-7f57d2a68af3': {
            'name': "The Republic",
        },
        '7bc691ca-c56f-4ace-abe3-c86ba2ed034b': {
            'name': "Player's Faction",
        }
    },
    'characters': {
        'bc3e6669-1587-42f9-a8d2-6f63b5461b32': {
            'name': "Giovanni Lagrange",
            'birth': '0001-01-01',
            'race': races.human,
            'gender': 'male',
            'faction': 'a4d7bb25-cad2-48b9-93ec-fbfe660a1331',
            'money': 1000
        },
        '5cf3a253-6d4b-4142-aa4b-00b180dbeeb0': {
            'name': "Kevin Smith",
            'birth': '0001-02-15',
            'race': races.human,
            'gender': 'male',
            'faction': '6ad7fd7c-e5c5-48de-a3d3-7f57d2a68af3',
            'money': 2000
        },
        '960d2522-9f5e-4b55-afe0-6f526199c33c': {
            'name': "Caesar Cardinal",
            'birth': '0002-07-19',
            'race': races.human,
            'gender': 'male',
            'faction': 'ef972728-c7ca-4c89-b459-0d25f6a6cb65',
            'money': 2500
        },
        '4d9c14ec-831e-444e-af37-97a5d4272f9b': {
            'name': "Azura Noenmodan",
            'birth': '0001-02-15',
            'race': races.human,
            'gender': 'female',
            'faction': '7bc691ca-c56f-4ace-abe3-c86ba2ed034b',
            'money': 15000
        },
        '38c760e1-ae64-47fd-95a1-501144f9ea5d': {
            'name': "Grallyren Morninggleam",
            'birth': '0011-11-23',
            'race': races.elf,
            'gender': 'female',
            'faction': '6ad7fd7c-e5c5-48de-a3d3-7f57d2a68af3',
            'money': 15000
        },
        '0ded64a7-43e3-4f23-a6ac-6068f343abe5': {
            'name': "Raenwynn Ashwalker",
            'birth': '0007-03-08',
            'race': races.elf,
            'gender': 'female',
            'faction': '7bc691ca-c56f-4ace-abe3-c86ba2ed034b',
            'money': 15000
        }
    },
    'provinces': {
        'hammer_peninsula': {
            'controller': 'bc3e6669-1587-42f9-a8d2-6f63b5461b32'
        },
        'iron_bay': {
            'controller': 'bc3e6669-1587-42f9-a8d2-6f63b5461b32'
        },
        'anvil_peninsula': {
            'controller': 'bc3e6669-1587-42f9-a8d2-6f63b5461b32'
        },
        'lowlands': {
            'controller': '5cf3a253-6d4b-4142-aa4b-00b180dbeeb0'
        },
        'hidden_wetlands': {
            'controller': '5cf3a253-6d4b-4142-aa4b-00b180dbeeb0'
        },
        'seabound_lowlands': {
            'controller': '960d2522-9f5e-4b55-afe0-6f526199c33c'
        },
        'upper_shield': {
            'controller': '4d9c14ec-831e-444e-af37-97a5d4272f9b'
        },
        'merchant_bay': {
            'controller': '4d9c14ec-831e-444e-af37-97a5d4272f9b'
        },
        'lower_shield': {
            'controller': '4d9c14ec-831e-444e-af37-97a5d4272f9b'
        },
        'devils_island': {
            'controller': '4d9c14ec-831e-444e-af37-97a5d4272f9b'
        },
        'foot_peninsula': {
            'controller': '4d9c14ec-831e-444e-af37-97a5d4272f9b'
        },
        'western_highlands': {
            'controller': '38c760e1-ae64-47fd-95a1-501144f9ea5d'
        },
        'eastern_highlands': {
            'controller': '38c760e1-ae64-47fd-95a1-501144f9ea5d'
        },
        'summer_wetlands': {
            'controller': '38c760e1-ae64-47fd-95a1-501144f9ea5d'
        },
        'sunrise_bay': {
            'controller': '38c760e1-ae64-47fd-95a1-501144f9ea5d'
        },
        'west_great_lakes': {
            'controller': '0ded64a7-43e3-4f23-a6ac-6068f343abe5'
        },
        'northern_wetlands': {
            'controller': '0ded64a7-43e3-4f23-a6ac-6068f343abe5'
        },
        'east_great_lakes': {
            'controller': '0ded64a7-43e3-4f23-a6ac-6068f343abe5'
        },
        'easternland': {
            'controller': '0ded64a7-43e3-4f23-a6ac-6068f343abe5'
        }
    },
    'player': {
        'character': '4d9c14ec-831e-444e-af37-97a5d4272f9b'
    }
}