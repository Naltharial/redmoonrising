// races.js

portraits = {
    'm_human_1': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male1_lg.png'
    },
    'm_human_2': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male2_lg.png'
    },
    'm_human_3': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male3_lg.png'
    },
    'm_human_4': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male4_lg.png'
    },
    'm_human_5': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male5_lg.png'
    },
    'm_human_6': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male7_lg.png'
    },
    'm_human_7': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male8_lg.png'
    },
    'm_human_8': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male10_lg.png'
    },
    'm_human_9': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male11_lg.png'
    },
    'm_human_10': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male12_lg.png'
    },
    'm_human_11': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male16_lg.png'
    },
    'm_human_12': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male17_lg.png'
    },
    'm_human_13': {
        'race': races.human,
        'gender': 'male',
        'filename': 'male18_lg.png'
    },
    'm_dwarf_1': {
        'race': races.dwarf,
        'gender': 'male',
        'filename': 'male6_lg.png'
    },
    'm_dwarf_2': {
        'race': races.dwarf,
        'gender': 'male',
        'filename': 'male9_lg.png'
    },
    'm_elf_1': {
        'race': races.elf,
        'gender': 'male',
        'filename': 'male13_lg.png'
    },
    'm_elf_2': {
        'race': races.elf,
        'gender': 'male',
        'filename': 'male14_lg.png'
    },
    'm_elf_3': {
        'race': races.elf,
        'gender': 'male',
        'filename': 'male15_lg.png'
    },
    'f_human_1': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female1_lg.png'
    },
    'f_human_2': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female2_lg.png'
    },
    'f_human_3': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female3_lg.png'
    },
    'f_human_4': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female4_lg.png'
    },
    'f_human_5': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female5_lg.png'
    },
    'f_human_6': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female6_lg.png'
    },
    'f_human_7': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female7_lg.png',
        'random': false
    },
    'f_human_8': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female8_lg.png'
    },
    'f_human_9': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female11_lg.png'
    },
    'f_human_10': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female12_lg.png'
    },
    'f_human_11': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female13_lg.png'
    },
    'f_human_12': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female15_lg.png'
    },
    'f_human_13': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female16_lg.png'
    },
    'f_human_14': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female18_lg.png'
    },
    'f_human_15': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female20_lg.png'
    },
    'f_human_16': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female21_lg.png'
    },
    'f_human_17': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female22_lg.png'
    },
    'f_human_18': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female23_lg.png'
    },
    'f_human_19': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female24_lg.png'
    },
    'f_human_20': {
        'race': races.human,
        'gender': 'female',
        'filename': 'female25_lg.png'
    },
    'f_elf_1': {
        'race': races.elf,
        'gender': 'female',
        'filename': 'female9_lg.png'
    },
    'f_elf_2': {
        'race': races.elf,
        'gender': 'female',
        'filename': 'female10_lg.png'
    },
    'f_elf_3': {
        'race': races.elf,
        'gender': 'female',
        'filename': 'female14_lg.png'
    },
    'f_elf_4': {
        'race': races.elf,
        'gender': 'female',
        'filename': 'female17_lg.png'
    },
    'f_elf_5': {
        'race': races.elf,
        'gender': 'female',
        'filename': 'female19_lg.png'
    },
    'f_dwarf_1': {
        'race': races.dwarf,
        'gender': 'female',
        'filename': 'female26_lg.png'
    },
    'f_dwarf_2': {
        'race': races.dwarf,
        'gender': 'female',
        'filename': 'female27_lg.png'
    }
}