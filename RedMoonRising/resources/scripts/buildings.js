﻿// Default functions
function alwaysShow(province) {
    return op.always;
}
function checkMoney(province) {
    return province.owner.money.gt(this.cost);
}

buildings = {
    'castle': {
        'cost': 1000,
        'show': alwaysShow.bind(this),
        'enable': checkMoney.bind(this)
    }
}