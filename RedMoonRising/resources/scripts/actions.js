﻿// Default functions
function alwaysShow(province, character) {
    return op.always;
}
function ownerShow(province, character) {
    return province.owner.equals(character);
}

actions = {
    'attack_province': {
        'name': "Attack province",
        'show': alwaysShow,
        'enable': alwaysShow,
        'action': function (province, character) {
            province.attack(character);
        }
    },
    'defend_province': {
        'name': "Defend province",
        'show': alwaysShow,
        'enable': ownerShow,
        'action': function (province, character) {
            province.defend(character);
        }
    }
}