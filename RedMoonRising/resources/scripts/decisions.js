﻿// Default functions
function alwaysShow(character) {
    return op.always;
}

decisions = {
    'earn_money': {
        'show': alwaysShow.bind(this),
        'enable': alwaysShow.bind(this),
        'action': function (character) {
            character.money.add(10000);
        }
    }
}