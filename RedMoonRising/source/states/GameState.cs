﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace RedMoonRising.source.states
{
    enum ActionState { NONE, PANNING };

    class GameState : GUIState, IDisposable
    {
        public map.Map Map
        {
            get;
            protected set;
        }
        public Guid PlayerId
        {
            get;
            protected set;
        }
        public actors.Characters Characters
        {
            get;
            protected set;
        }
        public actors.Factions Factions
        {
            get;
            protected set;
        }

        private float maxZoomIn = 0.75F;
        private float stepZoomIn = 0.95F;
        private float maxZoomOut = 3F;
        private float stepZoomOut = 1.05F;

        private ActionState actionState;

        private View gameView;
        private View guiView;

        private Vector2i panningAnchor;
        private float zoomLevel;

        private Dictionary<string, core.scripting.Action> currAct;

        public void Center(Vector2f pos)
        {

            guiView.Size = pos;
            gameView.Size = pos;
            pos *= 0.5f;
            guiView.Center = pos;
            zoomLevel = 1.0F;

            var centre = new Vector2f(Map.size * 0.5F, Map.size * 0.5F);
            gameView.Center = centre;

            gameView.Zoom(2);
            zoomLevel *= 2;
        }

        public override void Update(long dt)
        {
            
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            // Draw Game area
            target.SetView(gameView);

            Map.Draw(target, states);

            if (guiSystem["actionmap"].Visible)
            {
                guiSystem["actionmap"].Draw(target, states);
            }

            // Draw GUI area
            target.SetView(guiView);

            var top = (gui.elements.TopElement)guiSystem["top"];
            top.Draw(Characters[PlayerId].Portrait.Texture, target, states);

            if (Map.Selected != null)
            {
                var sel = (gui.elements.SelectedElement)guiSystem["selected"];
                sel.Draw(target, states);
            }


            DrawFPS(target, states);
        }

        #region Event Handling
        public override StateChange HandleInput(RenderWindow window, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Escape)
            {
                return new StateChange(typeof(menu.ExitDialog));
            }
            else
            {
                return null;
            }
        }
        public StateChange HandleInput(RenderWindow window, MouseMoveEventArgs e)
        {
            if (actionState == ActionState.PANNING)
            {
                var lim = new Vector2f(Map.size, Map.size);

                var mctp = window.MapCoordsToPixel(gameView.Center);
                Vector2f pos = (Vector2f)(Mouse.GetPosition(window) - panningAnchor);

                if (pos.X <= 0 && mctp.X >= lim.X)
                {
                    pos.X = 0;
                }
                if (pos.X >= 0 && mctp.X <= 0)
                {
                    pos.X = 0;
                }
                if (pos.Y <= 0 && mctp.Y >= lim.Y)
                {
                    pos.Y = 0;
                }
                if (pos.Y >= 0 && mctp.Y <= 0)
                {
                    pos.Y = 0;
                }

                gameView.Move(-1.0F * pos * zoomLevel);
                panningAnchor = Mouse.GetPosition(window);
            }

            return null;
        }
        public StateChange HandleInput(RenderWindow window, RedMoonRising.MouseButtonPressedEventArgs e)
        {
            if (e.Button == Mouse.Button.Left)
            {
                var ms = new Vector2u(Map.size, Map.size);
                var pos = Mouse.GetPosition(window);

                var mptcg = window.MapPixelToCoords(pos, guiView);
                var mptc = window.MapPixelToCoords(pos, gameView);

                // GUI check
                var topc = guiSystem["top"].CheckPosition(mptcg);
                if (topc)
                {
                    var topel = guiSystem["top"].GetEntryAt(mptcg);

                    if (topel != null)
                    {
                        Debug.WriteLine("[GAMESTATE] Selected " + topel.message);
                    }

                    return null;
                }
                else if (Map.Selected != null)
                {
                    if (guiSystem["actionmap"].Visible)
                    {
                        if (guiSystem["actionmap"].CheckPosition(mptc))
                        {
                            var selel = guiSystem["actionmap"].GetEntryAt(mptc);
                            Debug.WriteLine("[GAMESTATE] Selected actionmap for " + Map.Selected.Id + ", action " + selel.message);

                            var actsel = currAct[selel.message];
                            
                            if (actsel.Available)
                            {
                                actsel.Activate(Map.Selected, Characters[PlayerId]);
                            }

                            return null;
                        }
                    }

                    if (guiSystem["selected"].CheckPosition(mptcg))
                    {
                        var selel = guiSystem["selected"].GetEntryAt(mptcg);
                        Debug.WriteLine("[GAMESTATE] Selected select panel");

                        return null;
                    }
                }

                // Map check
                var tileCoord = new Vector2i((int)(mptc.X), (int)(mptc.Y));

                if (Map.size > tileCoord.X && tileCoord.X >= 0 && Map.size > tileCoord.Y && tileCoord.Y >= 0)
                {
                    var tt = Map.AtPosition((Vector2f)tileCoord);
                    var s = tt.Item2;

                    if (s != null)
                    {
                        Map.Select(s);
                        ((gui.elements.SelectedElement)guiSystem["selected"]).SetSelected(Map.Selected);
                        guiSystem["actionmap"].Visible = false;


                        Debug.WriteLine("[GAMESTATE] Selected tile: " + Map.Selected.Id);

                        if (tt.Item1)
                        {
                            Debug.WriteLine("[GAMESTATE] Selected actionmap");

                            var acmp = guiSystem["actionmap"];

                            currAct = actions.ActionFactory.GetActions(Map.Selected, Characters[PlayerId]);

                            var items = new List<Tuple<string, string>>();

                            foreach (var item in currAct)
                            {
                                items.Add(new Tuple<string, string>(item.Value.Name, item.Key));
                            }

                            acmp.SetEntries(items);
                            acmp.Visible = true;
                            var acs = acmp.GetSize();

                            // TODO: dynamic sizing
                            var acpos = new Vector2f(Map.Selected.Center.X + 25, Map.Selected.Center.Y - acs.Y / 2);
                            acmp.Position = acpos;
                        }
                    }
                    else
                    {
                        Debug.WriteLine("[GAMESTATE] Select tile failed");
                    }
                }
            }
            else if (e.Button == Mouse.Button.Middle)
            {
                if (actionState != ActionState.PANNING)
                {
                    actionState = ActionState.PANNING;
                    panningAnchor = Mouse.GetPosition(window);
                }
            }

            return null;
        }
        public StateChange HandleInput(RenderWindow window, RedMoonRising.MouseButtonReleasedEventArgs e)
        {
            if (e.Button == Mouse.Button.Middle)
            {
                actionState = ActionState.NONE;
            }
            else if (e.Button == Mouse.Button.Right)
            {
                Map.ClearSelect();
                guiSystem["selected"].Visible = false;
                guiSystem["actionmap"].Visible = false;
            }

            return null;
        }
        public StateChange HandleInput(RenderWindow window, MouseWheelEventArgs e)
        {
            if (e.Delta < 0)
            {
                if (zoomLevel > maxZoomOut) return null;
                gameView.Zoom(stepZoomOut);
                zoomLevel *= 1.05f;
            }
            else
            {
                if (zoomLevel < maxZoomIn) return null;
                gameView.Zoom(stepZoomIn);
                zoomLevel *= 0.95f;
            }

            return null;
        }
        #endregion

        public new void Dispose()
        {
            ((IDisposable)gameView).Dispose();
            ((IDisposable)guiView).Dispose();
        }

        private GameState()
            : base()
        {
            actionState = ActionState.NONE;

            guiView = new View();
            gameView = new View();
        }
        public GameState(Vector2f size, map.Map map)
            : this()
        {
            Map = map;

            Characters = new actors.Characters();
            var pl = actors.CharacterFactory.GeneratePlayer();
            Characters.Add(pl.Id, pl);
            PlayerId = pl.Id;

            Factions = new actors.Factions();
            var pf = new actors.Faction("Make #Country Great Again");
            Factions.Add(pf.Id, pf);

            var items = new List<string[]>();
            var mloc = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(resources.localization.Strings.money.ToLower());
            var lloc = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(resources.localization.Strings.character_level.ToLower());
            items.Add(new string[] { Characters[PlayerId].Money.ToString(), "money", mloc});
            items.Add(new string[] { Characters[PlayerId].Level.ToString(), "level", lloc });
            var top = new gui.elements.TopElement(size, items);
            top.Visible = true;
            guiSystem.Add("top", top);

            var sel = new gui.elements.SelectedElement(size);
            sel.Visible = true;
            guiSystem.Add("selected", sel);

            var act = new gui.elements.ActionmapElement(size);
            guiSystem.Add("actionmap", act);

            Center(size);
        }
        public GameState(Vector2f size, core.History history)
            : this(size, history.Map)
        {
            Characters = history.Characters;
            Factions = history.Factions;
            PlayerId = history.PlayerId;
        }
    }
}
