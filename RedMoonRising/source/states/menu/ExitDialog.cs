﻿using System;

using SFML.System;
using SFML.Window;
using SFML.Graphics;
using System.Collections.Generic;

namespace RedMoonRising.source.states.menu
{
    class ExitDialog : GUIState, IDisposable
    {
        public override StateChange HandleInput(RenderWindow window, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Escape)
            {
                return new StateChange(typeof(PopState));
            }
            else if (e.Code == Keyboard.Key.Return)
            {
                return new StateChange(typeof(ExitState));
            }
            else
            {
                return null;
            }
        }
        public virtual StateChange HandleInput(RenderWindow window, MouseMoveEventArgs e)
        {
            var mousePos = new Vector2f(e.X, e.Y);
            guiSystem["exit"].Highlight(guiSystem["exit"].GetEntryAt(mousePos));

            return null;
        }
        public virtual StateChange HandleInput(RenderWindow window, RedMoonRising.MouseButtonPressedEventArgs e)
        {
            if (e.Button == Mouse.Button.Left)
            {
                var msg = guiSystem["exit"].Activate(new Vector2f(e.X, e.Y));

                if (msg == "no")
                {
                    return new StateChange(typeof(PopState));
                }
                else if (msg == "yes")
                {
                    return new StateChange(typeof(ExitState));
                }

            }

            return null;
        }

        public ExitDialog()
        {
            var items = new List<Tuple<string, string>>();
            items.Add(new Tuple<string, string>("Yes", "yes"));
            items.Add(new Tuple<string, string>("No", "no"));
            var Gui = new gui.elements.MenuGUI(new Vector2f(128, 32), items);
            Gui.Horizontal = true;
            Gui.Title = "Exit Game";
            Gui.Instructions = "Are you sure you want to exit the game?";
            Gui.Visible = true;

            guiSystem.Add("exit", Gui);
        }
    }
}
