﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.states.menu
{
    class MainMenu : GUIState
    {
        public override StateChange HandleInput(RenderWindow window, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Escape)
            {
                return new StateChange(typeof(ExitDialog));
            }
            else
            {
                return null;
            }
        }
        public virtual StateChange HandleInput(RenderWindow window, MouseMoveEventArgs e)
        {
            var mousePos = new Vector2f(e.X, e.Y);
            guiSystem["menu"].Highlight(guiSystem["menu"].GetEntryAt(mousePos));

            return null;
        }
        public virtual StateChange HandleInput(RenderWindow window, RedMoonRising.MouseButtonPressedEventArgs e)
        {
            if (e.Button == Mouse.Button.Left)
            {
                var msg = guiSystem["menu"].Activate(new Vector2f(e.X, e.Y));

                if (msg == "new_game")
                {
                    return new StateChange(typeof(GameState));
                }
                else if (msg == "exit_game")
                {
                    return new StateChange(typeof(ExitDialog));
                }

            }

            return null;
        }

        public MainMenu()
        {
            var items = new List<Tuple<string, string>>();
            items.Add(new Tuple<string, string>("New Game", "new_game"));
            items.Add(new Tuple<string, string>("Load Game", "load_game"));
            items.Add(new Tuple<string, string>("", ""));
            items.Add(new Tuple<string, string>("Credits", "credits"));
            items.Add(new Tuple<string, string>("Exit", "exit_game"));

            var Gui = new gui.elements.MenuGUI(new Vector2f(192, 32), items);
            Gui.Visible = true;
            guiSystem.Add("menu", Gui);
        }
    }
}
