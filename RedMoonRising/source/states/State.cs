﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.states
{
    abstract class State : core.Drawn
    {
        private core.MaxConcurrentQueue<double> fps;
        private core.MaxConcurrentQueue<double> dfps;
        private Text fpsText;
        private readonly int sampleSize = 100;

        protected long ElapsedTime
        {
            get;
            private set;
        }

        public virtual StateChange HandleInput(RenderWindow window, EventArgs e)
        {
            return null;
        }

        public double GetFPS()
        {
            return fps.Aggregate((a,b) => a + b) / fps.Size;
        }
        public double GetDFPS()
        {
            return dfps.Aggregate((a, b) => a + b) / fps.Size;
        }

        public void UpdateDraw(RenderTarget target, RenderStates states, long dt)
        {
            dfps.Enqueue(1.0 / dt * 1000000.0);

            Draw(target, states);
        }

        public void UpdateState(long dt)
        {
            ElapsedTime += dt;
            fps.Enqueue(1.0 / dt * 1000000.0);

            Update(dt);
        }
        public virtual void Update(long dt)
        {

        }

        public virtual void DrawFPS(RenderTarget target, RenderStates states)
        {
            fpsText.DisplayedString = ((uint)GetDFPS()).ToString() + " (" + ((uint)GetFPS()).ToString() + ")";
            var lb = fpsText.GetLocalBounds();
            fpsText.Position = new Vector2f(target.Size.X - 45 - lb.Width, 20);

            fpsText.Draw(target, states);
        }

        public State()
        {
            var c = Color.White;
            c.A = 128;
            fpsText = new Text("", style.bodyFont, 20);
            fpsText.Color = c;

            fps = new core.MaxConcurrentQueue<double>(sampleSize);
            dfps = new core.MaxConcurrentQueue<double>(sampleSize);
        }
    }
}
