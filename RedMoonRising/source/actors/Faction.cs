﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.actors
{
    class Faction
    {
        public Guid Id
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        private Characters members;
        private map.TileAtlas tiles;
        private Dictionary<string, Guid> controllers;

        public void AddCharacter(Character character)
        {
            var el = members.ContainsKey(character.Id);
            if (!el)
            {
                members.Add(character.Id, character);
                character.Faction = this;
            }
        }
        public void RemoveCharacter(Guid character)
        {
            var el = members.ContainsKey(character);
            if (!el)
            {
                throw new ArgumentException("GUID not a member of faction.");
            }

            var ch = members[character];
            ch.Faction = null;
            members.Remove(character);
        }
        public void RemoveCharacter(Character character)
        {
            RemoveCharacter(character.Id);
        }

        public void AddTile(map.Tile tile)
        {
            var el = tiles.ContainsKey(tile.Id);
            if (!el)
            {
                tiles.Add(tile.Id, tile);
                tile.Owner = this;
            }
        }
        public void RemoveTile(string name)
        {
            var ch = tiles[name];
            ch.Owner = null;
            tiles.Remove(name);
        }
        public void RemoveTile(map.Tile tile)
        {
            RemoveTile(tile.Id);
        }

        public void AddController(Character member, map.Tile tile)
        {
            AddCharacter(member);
            AddTile(tile);

            controllers.Add(tile.Id, member.Id);
        }
        public Character GetController(map.Tile tile)
        {
            Guid ch;
            controllers.TryGetValue(tile.Id, out ch);

            if (ch != null)
            {
                return members[ch];
            }
            else
            {
                return null;
            }
        }

        public Faction(Guid id, string name)
        {
            Id = id;
            Name = name;

            members = new Characters();
            tiles = new map.TileAtlas();
            controllers = new Dictionary<string, Guid>();
        }
        public Faction(string name)
            : this(Guid.NewGuid(), name)
        {
            
        }
    }
}
