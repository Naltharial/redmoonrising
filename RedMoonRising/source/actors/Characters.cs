﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace RedMoonRising.source.actors
{
    class Characters : IDictionary<Guid, Character>
    {
        private IDictionary<Guid, Character> typeClass;
        
        public Characters()
        {
            typeClass = new Dictionary<Guid, Character>();
        }

        public void Add(Character value)
        {
            typeClass.Add(value.Id, value);
        }

        #region typeClass passthrough
        public Character this[Guid key]
        {
            get
            {
                return typeClass[key];
            }

            set
            {
                typeClass[key] = value;
            }
        }

        public int Count
        {
            get
            {
                return typeClass.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return typeClass.IsReadOnly;
            }
        }

        public ICollection<Guid> Keys
        {
            get
            {
                return typeClass.Keys;
            }
        }

        public ICollection<Character> Values
        {
            get
            {
                return typeClass.Values;
            }
        }

        public void Add(KeyValuePair<Guid, Character> item)
        {
            typeClass.Add(item);
        }

        public void Add(Guid key, Character value)
        {
            typeClass.Add(key, value);
        }

        public void Clear()
        {
            typeClass.Clear();
        }

        public bool Contains(KeyValuePair<Guid, Character> item)
        {
            return typeClass.Contains(item);
        }

        public bool ContainsKey(Guid key)
        {
            return typeClass.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<Guid, Character>[] array, int arrayIndex)
        {
            typeClass.CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<Guid, Character>> GetEnumerator()
        {
            return typeClass.GetEnumerator();
        }

        public bool Remove(KeyValuePair<Guid, Character> item)
        {
            return typeClass.Remove(item);
        }

        public bool Remove(Guid key)
        {
            return typeClass.Remove(key);
        }

        public bool TryGetValue(Guid key, out Character value)
        {
            return typeClass.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return typeClass.GetEnumerator();
        }
        #endregion
    }
}