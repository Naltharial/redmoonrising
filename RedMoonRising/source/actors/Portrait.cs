﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;

namespace RedMoonRising.source.actors
{
    class Portrait : IDisposable
    {
        public string Name
        {
            get;
            private set;
        }

        public string File
        {
            get;
            private set;
        }

        private Texture _texture;
        public Texture Texture
        {
            get
            {
                return _texture;
            }
        }

        public void Dispose()
        {
            ((IDisposable)_texture).Dispose();
        }

        public Portrait(string name, string file)
        {
            Name = name;
            File = file;

            _texture = new Texture("resources/art/portraits/" + file);
        }
    }
}
