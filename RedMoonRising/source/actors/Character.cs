﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;

namespace RedMoonRising.source.actors
{
    class Character
    {
        public Guid Id
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public string Gender
        {
            get;
            private set;
        }

        public string Race
        {
            get;
            private set;
        }

        public DateTime Birth
        {
            get;
            private set;
        }

        public Portrait Portrait
        {
            get;
            private set;
        }

        public Faction Faction
        {
            get;
            set;
        }

        public decimal Money
        {
            get;
            set;
        }

        public uint Level
        {
            get;
            set;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}, {2}, born {3}", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Race), Name, Gender, Birth.ToShortDateString());
        }

        public Character(Guid id, string name, string gender, string race, DateTime birth, Portrait portrait)
        {
            Id = id;
            Name = name;
            Gender = gender;
            Race = race;

            Birth = birth;

            Portrait = portrait;

            Level = 1;
        }
    }
}
