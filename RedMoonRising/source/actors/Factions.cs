﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace RedMoonRising.source.actors
{
    class Factions : IDictionary<Guid, Faction>
    {
        private IDictionary<Guid, Faction> typeClass;
        
        public Factions()
        {
            typeClass = new Dictionary<Guid, Faction>();
        }

        public void Add(Faction value)
        {
            typeClass.Add(value.Id, value);
        }

        #region typeClass passthrough
        public Faction this[Guid key]
        {
            get
            {
                return typeClass[key];
            }

            set
            {
                typeClass[key] = value;
            }
        }

        public int Count
        {
            get
            {
                return typeClass.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return typeClass.IsReadOnly;
            }
        }

        public ICollection<Guid> Keys
        {
            get
            {
                return typeClass.Keys;
            }
        }

        public ICollection<Faction> Values
        {
            get
            {
                return typeClass.Values;
            }
        }

        public void Add(KeyValuePair<Guid, Faction> item)
        {
            typeClass.Add(item);
        }

        public void Add(Guid key, Faction value)
        {
            typeClass.Add(key, value);
        }

        public void Clear()
        {
            typeClass.Clear();
        }

        public bool Contains(KeyValuePair<Guid, Faction> item)
        {
            return typeClass.Contains(item);
        }

        public bool ContainsKey(Guid key)
        {
            return typeClass.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<Guid, Faction>[] array, int arrayIndex)
        {
            typeClass.CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<Guid, Faction>> GetEnumerator()
        {
            return typeClass.GetEnumerator();
        }

        public bool Remove(KeyValuePair<Guid, Faction> item)
        {
            return typeClass.Remove(item);
        }

        public bool Remove(Guid key)
        {
            return typeClass.Remove(key);
        }

        public bool TryGetValue(Guid key, out Faction value)
        {
            return typeClass.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return typeClass.GetEnumerator();
        }
        #endregion
    }
}