﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.actors
{
    static class CharacterFactory
    {
        private static Jurassic.ScriptEngine engine;
        
        private static string jsr = "resources/scripts/races.js";
        private static string jst = "resources/scripts/traits.js";
        private static string jsp = "resources/scripts/portraits.js";

        private static string startDate = "0001-01-01";

        private static string playerPortrait = "f_human_7";

        private static core.DistributedRandom ren;

        public static Character GenerateRandom()
        {
            var races = engine.GetGlobalValue<Jurassic.Library.ObjectInstance>("races");

            var gender = "female";
            if (ren.NextDouble() > 0.5)
            {
                gender = "male";
            }
            
            var wrace = new SortedDictionary<double, string>();
            var wname = new SortedDictionary<string, string>();
            foreach (var item in races.Properties)
            {
                var rc = (Jurassic.Library.ObjectInstance)item.Value;
                wrace.Add(Convert.ToSingle(rc["rarity"]), item.Name);

                var nl = (Jurassic.Library.ObjectInstance)rc["names"];
                var fl = (Jurassic.Library.ArrayInstance)nl[gender];
                var ll = (Jurassic.Library.ArrayInstance)nl["last"];

                wname.Add(item.Name, fl[ren.Next((int)fl.Length)] + " " + ll[ren.Next((int)ll.Length)]);
            }

            var race = wrace.Values.First();
            var rchoice = ren.NextDouble();
            foreach (var item in wrace)
            {
                if (item.Key >= rchoice)
                {
                    race = item.Value;
                    break;
                }
                else
                {
                    rchoice -= item.Key;
                }
            }

            var portraits = engine.GetGlobalValue<Jurassic.Library.ObjectInstance>("portraits");
            var pl = new List<string>();
            foreach (var item in portraits.Properties)
            {
                var rc = (Jurassic.Library.ObjectInstance)item.Value;
                var prace = (Jurassic.Library.ObjectInstance)rc["race"];
                var pgender = (string)rc["gender"];

                if ((string)prace["name"] == race && pgender == gender)
                {
                    pl.Add(item.Name);
                }
            }

            var bd = Convert.ToDateTime(startDate);
            bd = bd.AddDays(ren.Next(30));
            bd = bd.AddMonths(ren.Next(12));
            var maxage = Convert.ToInt32(((Jurassic.Library.ObjectInstance)races[race])["age"]);
            bd = bd.AddYears(ren.NextNormal(maxage));

            var pname = pl[ren.Next(pl.Count)];
            var portrait = new Portrait(pname, (string)((Jurassic.Library.ObjectInstance)portraits[pname])["filename"]);
            Character c = new Character(Guid.NewGuid(), wname[race], gender, race, bd, portrait);

            Debug.WriteLine("[CHARACTERFACTORY] Generated random " + c);
            return c;
        }

        public static Character GeneratePlayer()
        {
            var portraits = engine.GetGlobalValue<Jurassic.Library.ObjectInstance>("portraits");
            var fn = "";
            foreach (var item in portraits.Properties)
            {
                if (item.Name == playerPortrait)
                {
                    fn = (string)((Jurassic.Library.ObjectInstance)portraits[item.Name])["filename"];
                }
            }
            var portrait = new Portrait(playerPortrait, fn);

            var dt = Convert.ToDateTime(startDate);
            var gi = Guid.NewGuid();
            var c = new Character(Guid.NewGuid(), "Player McPlayerness von Playerburg", "female", "human", dt, portrait);
            
            // SHOW ME THE MONEY
            c.Money = 1000000;

            Debug.WriteLine("[CHARACTERFACTORY] Generated player " + c);

            return c;
        }

        public static Portrait GetRandomPortrait(string race, string gender)
        {
            var portraits = engine.GetGlobalValue<Jurassic.Library.ObjectInstance>("portraits");
            var pl = new List<string>();
            foreach (var item in portraits.Properties)
            {
                var rc = (Jurassic.Library.ObjectInstance)item.Value;
                var prace = (Jurassic.Library.ObjectInstance)rc["race"];
                var pgender = (string)rc["gender"];

                if ((string)prace["name"] == race && pgender == gender)
                {
                    pl.Add(item.Name);
                }
            }

            var pname = pl[ren.Next(pl.Count)];
            return new Portrait(pname, (string)((Jurassic.Library.ObjectInstance)portraits[pname])["filename"]);
        }
        public static Portrait GetPlayerPortrait()
        {
            var portraits = engine.GetGlobalValue<Jurassic.Library.ObjectInstance>("portraits");
            var fn = "";
            foreach (var item in portraits.Properties)
            {
                if (item.Name == playerPortrait)
                {
                    fn = (string)((Jurassic.Library.ObjectInstance)portraits[item.Name])["filename"];
                }
            }
            return new Portrait(playerPortrait, fn);
        }

        static CharacterFactory()
        {
            engine = core.scripting.JEngineFactory.GetJEngine(new string[] { jsr, jst, jsp });

            ren = new core.DistributedRandom();
        }
    }
}
