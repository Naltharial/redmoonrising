﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.System;
using SFML.Graphics;

namespace RedMoonRising.source.gui
{
    abstract class GUI : core.Drawn
    {
        public abstract bool Visible
        {
            get;
            set;
        }

        public abstract Vector2f Dimensions
        {
            get;
            set;
        }

        public abstract new Vector2f Position
        {
            get;
            set;
        }

        private bool horizontal;
        public virtual bool Horizontal
        {
            get
            {
                return horizontal;
            }
            set
            {
                horizontal = value;
            }
        }

        // Not thread-safe
        protected List<GUIEntry> entries;

        // Content has been changed since last position update
        protected bool dirty = true;

        protected Shape bgShape;

        public virtual Vector2f GetSize()
        {
            if (Horizontal)
            {
                return new Vector2f(Dimensions.X * entries.Count, Dimensions.Y);
            }
            else
            {
                return new Vector2f(Dimensions.X, Dimensions.Y * entries.Count);
            }
        }

        public virtual bool CheckPosition(Vector2f mousePos)
        {
            uint i, j, nvert = bgShape.GetPointCount();
            bool c = false;

            for (i = 0, j = nvert - 1; i < nvert; j = i++)
            {
                if (((bgShape.GetPoint(i).Y >= mousePos.Y) != (bgShape.GetPoint(j).Y >= mousePos.Y)) &&
                        (mousePos.X <= (bgShape.GetPoint(j).X - bgShape.GetPoint(i).X) *
                        (mousePos.Y - bgShape.GetPoint(i).Y) /
                        (bgShape.GetPoint(j).Y - bgShape.GetPoint(i).Y) + bgShape.GetPoint(i).X))
                {
                    c = !c;
                }
            }

            return c;
        }

        public virtual void SetEntries(List<Tuple<string, string>> entries)
        {
            var shape = new RectangleShape();
            shape.Size = Dimensions;
            shape.FillColor = style.GetColor(0);
            shape.OutlineThickness = -style.border;
            shape.OutlineColor = style.GetColor(1);

            lock (entries)
            {
                this.entries.Clear();
                foreach (var entry in entries)
                {
                    var text = new Text();
                    text.DisplayedString = entry.Item1;
                    text.Font = style.bodyFont;
                    text.Color = style.GetColor(1);
                    text.CharacterSize = style.itemSize;

                    this.entries.Add(new GUIEntry(entry.Item2, new RectangleShape(shape), text));
                }
            }

            dirty = true;
        }

        public virtual GUIEntry GetEntryAt(Vector2f mousePos)
        {
            if (entries.Count == 0) return null;
            if (!Visible) return null;

            for (int i = 0; i < entries.Count; ++i)
            {
                Vector2f point = mousePos;
                point += entries[i].shape.Origin;
                point -= entries[i].shape.Position;

                if (point.X < 0 || point.X > entries[i].shape.Scale.X * Dimensions.X) continue;
                if (point.Y < 0 || point.Y > entries[i].shape.Scale.Y * Dimensions.Y) continue;
                return entries[i];
            }

            return null;
        }

        public virtual void Highlight(GUIEntry entry)
        {
            for (int i = 0; i < entries.Count; ++i)
            {
                if (entries[i] == entry && entries[i].message != "")
                {
                    entries[i].shape.FillColor = style.GetColor(1);
                    entries[i].shape.OutlineColor = style.GetColor(2);
                    entries[i].text.Color = style.GetColor(2);
                }
                else
                {
                    entries[i].shape.FillColor = style.GetColor(0);
                    entries[i].shape.OutlineColor = style.GetColor(1);
                    entries[i].text.Color = style.GetColor(1);
                }
            }
        }

        public void SetEntryText(GUIEntry entry, string text)
        {
            var n = entries.FindIndex(x => x == entry);
            entries[n].text.DisplayedString = text;
        }

        public virtual string Activate(GUIEntry entry)
        {
            var n = entries.FindIndex(x => x == entry);

            if (n < 0)
            {
                return "";
            }

            return entries[n].message;
        }
        public virtual string Activate(Vector2f mousePos)
        {
            var entry = GetEntryAt(mousePos);
            return Activate(entry);
        }

        public GUI()
        {
            horizontal = false;
        }
        public GUI(bool horizontal)
        {
            this.horizontal = horizontal;
        }
    }
}
