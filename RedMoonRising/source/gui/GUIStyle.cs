﻿using System;
using System.Collections.Generic;

using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace RedMoonRising.source.gui
{
    public sealed class GUIStyle : IDisposable
    {
        public Font bodyFont;
        public Font titleFont;

        public uint titleSize = 24;
        public uint itemSize = 20;
        public uint bodySize = 18;

        public int border = 3;
        public int tileBorder = 4;
        public int tileSBorder = 7;

        public int tileColor = 4;
        public int tileCenterColor = 5;

        private List<Color> cgp;

        public GUIStyle()
        {
            bodyFont = new Font("resources/fonts/cat_childs/CATChilds.ttf");
            titleFont = new Font("resources/fonts/avara/Avara.otf");

            cgp = new List<Color>();
            // Teal
            cgp.Add(new Color(64, 128, 128));
            // Light Teal
            cgp.Add(new Color(164, 228, 228));
            // White
            cgp.Add(new Color(255, 255, 255));
            // Gold
            cgp.Add(new Color(255, 215, 75));
            // Black
            cgp.Add(new Color(0, 0, 0));
            // Midnight Blue
            cgp.Add(new Color(68, 68, 122));
        }

        public void Dispose()
        {
            bodyFont.Dispose();
            titleFont.Dispose();
        }

        public Color GetColor(int index, byte alpha=255)
        {
            var cr =  new Color(cgp[index]);
            cr.A = alpha;

            return cr;
        }
    }
}
