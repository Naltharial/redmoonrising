﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;

using SFML.System;

namespace RedMoonRising.source.gui.elements
{
    class HUD : IDisposable
    {
        public static readonly gui.GUIStyle style = new gui.GUIStyle();

        private Vector2f size;

        private IDictionary<string, Shape> shapes;

        private Text t0;
        private Text t1;

        public void Draw(map.Tile selected, RenderTarget target, RenderStates states)
        {
            shapes["top"].Draw(target, states);

            if (selected != null)
            {
                shapes["selected"].Draw(target, states);
                var pos = new Vector2f(25, size.Y - 350);
                pos.Y += 20;
            }
        }

        public void Dispose()
        {
            ((IDisposable)t0).Dispose();
            ((IDisposable)t1).Dispose();
            ((IDisposable)style).Dispose();
        }

        public HUD(Vector2f size)
        {
            shapes = new Dictionary<string, Shape>();
            this.size = size;

            var topBg = new RectangleShape();
            topBg.Size = new Vector2f(size.X + 2 * style.border, 75);
            topBg.Position = new Vector2f(-style.border, -style.border);
            topBg.FillColor = style.GetColor(0);
            topBg.OutlineThickness = -style.border;
            topBg.OutlineColor = style.GetColor(1);
            shapes.Add("top", topBg);


            var selBg = new RectangleShape();
            selBg.Size = new Vector2f(400, 600);
            selBg.Position = new Vector2f(-style.border, size.Y - 600 + style.border);
            selBg.FillColor = style.GetColor(0);
            selBg.OutlineThickness = -style.border;
            selBg.OutlineColor = style.GetColor(1);
            shapes.Add("selected", selBg);
            
            t0 = new Text("", style.titleFont, style.bodySize);
            t1 = new Text("", style.bodyFont, style.bodySize);
        }
    }
}
