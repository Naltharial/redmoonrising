﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.gui.elements
{
    class ActionmapElement : GUI
    {
        private Vector2f position;
        public override Vector2f Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;

                bgShape.Position = position;

                foreach (var entry in entries)
                {
                    entry.shape.Position = Position;
                    entry.text.Position = Position;
                }
            }
        }

        private Vector2f dimensions;
        public override Vector2f Dimensions
        {
            get
            {
                return dimensions;
            }
            set
            {
                dimensions = value;

                foreach (var entry in entries)
                {
                    entry.shape.Size = value;
                    entry.text.CharacterSize = style.itemSize;
                }
            }
        }

        private bool visible;
        public override bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                if (value)
                {
                    var offset = new Vector2f(0.0F, style.border);

                    foreach (var entry in entries)
                    {
                        entry.shape.Origin = Origin - offset;

                        var ts = entry.text.GetLocalBounds();
                        var xoffs = (entry.shape.Size.X - 2 * style.border) / 2 - ts.Width / 2 + style.border;
                        var yoffs = (entry.shape.Size.Y - 2 * style.border) / 2 - ts.Height / 2 - style.border;
                        entry.text.Origin = Origin - offset - new Vector2f(xoffs, yoffs);

                        offset.Y += dimensions.Y;
                    }

                    dirty = false;
                }

                visible = value;
            }
        }

        public override bool CheckPosition(Vector2f mousePos)
        {
            var sz = ((RectangleShape)bgShape).Size;

            var x = mousePos.X >= bgShape.Position.X && mousePos.X <= bgShape.Position.X + sz.X;
            var y = mousePos.Y >= bgShape.Position.Y && mousePos.Y <= bgShape.Position.Y + sz.Y;

            return x && y;
        }

        public override void SetEntries(List<Tuple<string, string>> entries)
        {
            ((RectangleShape)bgShape).Size = new Vector2f(Dimensions.X, entries.Count * Dimensions.Y);

            base.SetEntries(entries);
        }

        public void Draw(map.Tile selected, RenderTarget target, RenderStates states)
        {
            if (dirty && Visible)
            {
                Visible = true;
            }
            if (!visible) return;

            Draw(target, states);
        }
        public override void Draw(RenderTarget target, RenderStates states)
        {
            if (dirty && Visible)
            {
                Visible = true;
            }
            if (!visible) return;
            
            bgShape.Draw(target, states);

            // Do not lock draw thread for a single item
            if (Monitor.TryEnter(entries))
            {
                try
                {
                    foreach (var entry in entries)
                    {
                        target.Draw(entry.shape);
                        target.Draw(entry.text);
                    }
                }
                finally
                {
                    Monitor.Exit(entries);
                }
            }
        }
        
        public ActionmapElement()
        {
            visible = false;

            entries = new List<GUIEntry>();
        }
        public ActionmapElement(Vector2f dims)
            : this()
        {
            bgShape = new RectangleShape();

            dimensions = new Vector2f(300, 35);

            bgShape = new RectangleShape();
            bgShape.Position = new Vector2f(0, dims.Y - 500);
            bgShape.FillColor = style.GetColor(0);
            bgShape.OutlineThickness = -style.border;
            bgShape.OutlineColor = style.GetColor(1);
        }
    }
}
