﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.gui.elements
{
    class TopElement : GUI
    {
        private Vector2f position;
        public override Vector2f Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;

                foreach (var entry in entries)
                {
                    entry.shape.Position = Position;
                    entry.text.Position = Position;
                }
            }
        }

        private Vector2f dimensions;
        public override Vector2f Dimensions
        {
            get
            {
                return dimensions;
            }
            set
            {
                dimensions = value;

                foreach (var entry in entries)
                {
                    entry.shape.Size = value;
                    entry.text.CharacterSize = style.itemSize;
                }
            }
        }

        private bool visible;
        public override bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                if (value)
                {
                    var offset = new Vector2f(0.0F, 0.0F);

                    foreach (var entry in entries)
                    {
                        entry.shape.Origin = Origin - offset;

                        var ts = entry.text.GetLocalBounds();
                        var xoffs = (entry.shape.Size.X - 2 * style.border) / 2 - ts.Width / 2 + style.border;
                        var yoffs = (entry.shape.Size.Y - 2 * style.border) / 2 - ts.Height / 2 - style.border;
                        entry.text.Origin = Origin - offset - new Vector2f(xoffs, yoffs);

                        offset.X += dimensions.X + 2 * style.border + 10;
                    }

                    dirty = false;
                }

                visible = value;
            }
        }

        private VertexArray rawShape;
        private new VertexArray bgShape;
        private VertexArray bgBorder;

        private GUIEntry portEntry;
        private Sprite charPortrait;

        public override bool CheckPosition(Vector2f mousePos)
        {
            uint i, j, nvert = rawShape.VertexCount;
            bool c = false;

            for (i = 0, j = nvert - 1; i < nvert; j = i++)
            {
                if (((rawShape[i].Position.Y >= mousePos.Y) != (rawShape[j].Position.Y >= mousePos.Y)) &&
                        (mousePos.X <= (rawShape[j].Position.X - rawShape[i].Position.X) *
                        (mousePos.Y - rawShape[i].Position.Y) /
                        (rawShape[j].Position.Y - rawShape[i].Position.Y) + rawShape[i].Position.X))
                {
                    c = !c;
                }
            }

            return c;
        }

        public override GUIEntry GetEntryAt(Vector2f mousePos)
        {
            if (entries.Count == 0) return null;
            if (!Visible) return null;

            Vector2f point = mousePos;
            point += charPortrait.Origin;
            point -= charPortrait.Position;

            if (point.X <= charPortrait.Scale.X * charPortrait.Texture.Size.X &&
                point.Y <= charPortrait.Scale.Y * charPortrait.Texture.Size.Y &&
                point.Y > 0 && point.X > 0)
            {
                return portEntry;
            }

            return base.GetEntryAt(mousePos);
        }

        public void Draw(Texture portrait, RenderTarget target, RenderStates states)
        {
            if (dirty && Visible)
            {
                Visible = true;
            }
            if (!visible) return;

            Draw(target, states);

            charPortrait.Texture = portrait;
            charPortrait.Position = new Vector2f(30, 10);
            charPortrait.Scale = new Vector2f(0.4f, 0.4f);
            charPortrait.Draw(target, states);
        }
        public override void Draw(RenderTarget target, RenderStates states)
        {
            if (dirty && Visible)
            {
                Visible = true;
            }
            if (!visible) return;

            bgBorder.Draw(target, states);
            bgShape.Draw(target, states);

            foreach (var entry in entries)
            {
                target.Draw(entry.shape);
                target.Draw(entry.text);
            }
        }
        
        private VertexArray AssembleRaw(Vector2f dimensions)
        {
            var retShape = new VertexArray(PrimitiveType.Triangles, 8);
            retShape.Append(new Vertex(new Vector2f(0, 0), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(dimensions.X, 0), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(dimensions.X, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(145, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(125, 155), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(25, 155), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(5, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(0, 70), style.GetColor(0)));

            return retShape;
        }
        private VertexArray AssembleBG(Vector2f dimensions)
        {
            var retShape = new VertexArray(PrimitiveType.Triangles, 18);
            retShape.Append(new Vertex(new Vector2f(0, 0), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(dimensions.X, 0), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(0, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(dimensions.X, 0), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(dimensions.X, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(0, 70), style.GetColor(0)));

            retShape.Append(new Vertex(new Vector2f(25, 0), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(125, 0), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(25, 155), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(125, 0), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(125, 155), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(25, 155), style.GetColor(0)));

            retShape.Append(new Vertex(new Vector2f(145, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(125, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(125, 155), style.GetColor(0)));

            retShape.Append(new Vertex(new Vector2f(5, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(25, 70), style.GetColor(0)));
            retShape.Append(new Vertex(new Vector2f(25, 155), style.GetColor(0)));

            return retShape;
        }
        private VertexArray AssembleBorder(Vector2f dimensions)
        {
            var retShape = new VertexArray(PrimitiveType.Triangles, 18);
            retShape.Append(new Vertex(new Vector2f(0, 0), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(dimensions.X, 0), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(0, 70 + style.border), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(dimensions.X, 0), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(dimensions.X, 70 + style.border), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(0, 70 + style.border), style.GetColor(1)));

            retShape.Append(new Vertex(new Vector2f(25 - style.border, 0), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(125 + style.border, 0), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(25 - style.border, 155 + style.border), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(125 + style.border, 0), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(125 + style.border, 155 + style.border), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(25 - style.border, 155 + style.border), style.GetColor(1)));

            retShape.Append(new Vertex(new Vector2f(145 + style.border, 70), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(125, 70 + style.border), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(125 + style.border, 155 + style.border), style.GetColor(1)));

            retShape.Append(new Vertex(new Vector2f(5 - style.border, 70), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(25 - style.border, 70), style.GetColor(1)));
            retShape.Append(new Vertex(new Vector2f(25 - style.border, 155 + style.border), style.GetColor(1)));

            return retShape;
        }

        public TopElement()
            :base(true)
        {
            charPortrait = new Sprite();

            entries = new List<GUIEntry>();
        }
        public TopElement(Vector2f dims)
            : this()
        {
            rawShape = AssembleRaw(dims);
            bgShape = AssembleBG(dims);
            bgBorder = AssembleBorder(dims);

            dimensions = new Vector2f(150, 50);
            Origin = new Vector2f(-170 - style.border, -5 - style.border);
        }
        public TopElement(Vector2f dims, List<Tuple<string, string>> entries)
           : this(dims)
        {
            var shape = new RectangleShape();
            shape.Size = new Vector2f(150, 50);
            shape.FillColor = style.GetColor(0);
            shape.OutlineThickness = -style.border;
            shape.OutlineColor = style.GetColor(1);

            portEntry = new GUIEntry();
            portEntry.message = "portrait";

            foreach (var entry in entries)
            {
                var text = new Text();
                text.DisplayedString = entry.Item1;
                text.Font = style.bodyFont;
                text.Color = style.GetColor(1);
                text.CharacterSize = style.itemSize;

                this.entries.Add(new GUIEntry(entry.Item2, new RectangleShape(shape), text));
            }

        }
        public TopElement(Vector2f dims, List<string[]> entries)
           : this(dims)
        {
            var shape = new RectangleShape();
            shape.Size = new Vector2f(150, 50);
            shape.FillColor = style.GetColor(0);
            shape.OutlineThickness = -style.border;
            shape.OutlineColor = style.GetColor(1);

            portEntry = new GUIEntry();
            portEntry.message = "portrait";

            foreach (var entry in entries)
            {
                var text = new Text();
                text.DisplayedString = entry[2] + ": " + entry[0];
                text.Font = style.bodyFont;
                text.Color = style.GetColor(1);
                text.CharacterSize = style.itemSize;

                this.entries.Add(new GUIEntry(entry[1], new RectangleShape(shape), text));
            }

        }
    }
}
