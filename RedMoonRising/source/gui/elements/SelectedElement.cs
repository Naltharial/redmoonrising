﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.gui.elements
{
    class SelectedElement : GUI
    {
        private Vector2f position;
        public override Vector2f Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;

                foreach (var entry in entries)
                {
                    entry.shape.Position = Position;
                    entry.text.Position = Position;
                }
            }
        }

        private Vector2f dimensions;
        public override Vector2f Dimensions
        {
            get
            {
                return dimensions;
            }
            set
            {
                dimensions = value;

                foreach (var entry in entries)
                {
                    entry.shape.Size = value;
                    entry.text.CharacterSize = style.itemSize;
                }
            }
        }

        private bool visible;
        public override bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                if (value)
                {
                    var offset = new Vector2f(-style.border * 4, 0.0F);

                    if (selected != null)
                    {
                        name.DisplayedString = selected.Name;
                        faction.DisplayedString = selected.Owner.Name;

                        name.Origin = offset;
                        offset.Y -= style.border * 2 + 20;

                        faction.Origin = offset;
                        offset.Y -= style.border * 2 + 20;
                    }

                    foreach (var entry in entries)
                    {
                        entry.shape.Origin = Origin - offset;

                        var ts = entry.text.GetLocalBounds();
                        var xoffs = (entry.shape.Size.X - 2 * style.border) / 2 - ts.Width / 2 + style.border;
                        var yoffs = (entry.shape.Size.Y - 2 * style.border) / 2 - ts.Height / 2 - style.border;
                        entry.text.Origin = Origin - offset - new Vector2f(xoffs, yoffs);

                        offset.X += dimensions.X + 2 * style.border + 10;
                    }

                    dirty = false;
                }

                visible = value;
            }
        }

        private map.Tile selected;
        private Text name;
        private Text faction;

        public override bool CheckPosition(Vector2f mousePos)
        {
            uint i, j, nvert = bgShape.GetPointCount();
            bool c = false;

            for (i = 0, j = nvert - 1; i < nvert; j = i++)
            {
                if (((bgShape.GetPoint(i).Y >= mousePos.Y) != (bgShape.GetPoint(j).Y >= mousePos.Y)) &&
                        (mousePos.X <= (bgShape.GetPoint(j).X - bgShape.GetPoint(i).X) *
                        (mousePos.Y - bgShape.GetPoint(i).Y) /
                        (bgShape.GetPoint(j).Y - bgShape.GetPoint(i).Y) + bgShape.GetPoint(i).X))
                {
                    c = !c;
                }
            }

            return c;
        }

        public void SetSelected(map.Tile selected)
        {
            this.selected = selected;
            this.Visible = true;
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            if (dirty && Visible)
            {
                Visible = true;
            }
            if (!visible) return;
            
            bgShape.Draw(target, states);

            if (selected != null)
            {
                name.Draw(target, states);
                faction.Draw(target, states);
            }

            foreach (var entry in entries)
            {
                target.Draw(entry.shape);
                target.Draw(entry.text);
            }
        }
        
        public SelectedElement()
        {
            visible = false;

            entries = new List<GUIEntry>();

            name = new Text("name", style.titleFont, 20);
            faction = new Text("faction", style.bodyFont, 16);
        }
        public SelectedElement(Vector2f dims)
            : this()
        {
            bgShape = new RectangleShape();

            dimensions = new Vector2f(150, 50);

            bgShape = new RectangleShape(new Vector2f(450, 500));
            bgShape.Position = new Vector2f(0, dims.Y - 500);
            bgShape.FillColor = style.GetColor(0);
            bgShape.OutlineThickness = -style.border;
            bgShape.OutlineColor = style.GetColor(1);

            name.Position = bgShape.Position + new Vector2f(0, 15);
            faction.Position = bgShape.Position + new Vector2f(0, 20);
        }
        public SelectedElement(Vector2f dims, List<Tuple<string, string>> entries)
           : this(dims)
        {
            var shape = new RectangleShape();
            shape.Size = new Vector2f(150, 50);
            shape.FillColor = style.GetColor(0);
            shape.OutlineThickness = -style.border;
            shape.OutlineColor = style.GetColor(1);

            foreach (var entry in entries)
            {
                var text = new Text();
                text.DisplayedString = entry.Item1;
                text.Font = style.bodyFont;
                text.Color = style.GetColor(1);
                text.CharacterSize = style.itemSize;

                this.entries.Add(new GUIEntry(entry.Item2, new RectangleShape(shape), text));
            }

        }
    }
}
