﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.gui.elements
{
    class MenuGUI : GUI
    {
        private bool horizontal;
        public override bool Horizontal
        {
            get
            {
                return horizontal;
            }
            set
            {
                horizontal = value;

                Title = title;
                Instructions = instructions;
            }
        }

        private Vector2f position;
        public override Vector2f Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;

                if (!string.IsNullOrEmpty(Title))
                {
                    titleText.Position = Position;
                }
                if (!string.IsNullOrEmpty(Instructions))
                {
                    instrText.Position = Position;
                }

                foreach (var entry in entries)
                {
                    entry.shape.Position = Position;
                    entry.text.Position = Position;
                }
            }
        }

        private Vector2f dimensions;
        public override Vector2f Dimensions
        {
            get
            {
                return dimensions;
            }
            set
            {
                dimensions = value;

                foreach (var entry in entries)
                {
                    entry.shape.Size = value;
                    entry.text.CharacterSize = style.itemSize;
                }
            }
        }

        private bool visible;
        public override bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                if (value)
                {
                    var texts = 0.0F;
                    if (!string.IsNullOrEmpty(Title))
                    {
                        texts += titleText.GetLocalBounds().Height;
                        texts += style.border * 4;
                    }
                    var offset = new Vector2f(0.0F, texts);
                    if (!string.IsNullOrEmpty(Instructions))
                    {
                        if (!string.IsNullOrEmpty(Title))
                        {
                            instrText.Origin = -offset;
                        }
                        texts += instrText.GetLocalBounds().Height;
                        texts += style.border * 4;
                    }
                    offset.Y = texts;

                    foreach (var entry in entries)
                    {
                        entry.shape.Origin = Origin - offset;

                        var ts = entry.text.GetLocalBounds();
                        var xoffs = (entry.shape.Size.X - 2 * style.border) / 2 - ts.Width / 2 + style.border;
                        entry.text.Origin = Origin - offset - new Vector2f(xoffs, 0);

                        if (Horizontal) offset.X += dimensions.X;
                        else offset.Y += dimensions.Y;
                    }

                    dirty = false;
                }

                visible = value;
            }
        }

        private string title = "";
        private Text titleText;
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                var rtit = GUIEntry.CutToSize(value, style.titleFont, style.titleSize, GetSize().X);
                titleText = new Text(rtit, style.titleFont, style.titleSize);
                titleText.Color = style.GetColor(1);

                title = value;
                dirty = true;
            }
        }

        private string instructions = "";
        private Text instrText;
        public string Instructions
        {
            get
            {
                return instructions;
            }
            set
            {
                var rinstr = GUIEntry.CutToSize(value, style.bodyFont, style.bodySize, GetSize().X);
                instrText = new Text(rinstr, style.bodyFont, style.bodySize);
                instrText.Color = style.GetColor(1);

                instructions = value;
                dirty = true;
            }
        }

        public override Vector2f GetSize()
        {
            var texts = 0.0F;
            if (!string.IsNullOrEmpty(Title))
            {
                texts += titleText.GetLocalBounds().Height;
                texts += style.border * 2;
            }
            if (!string.IsNullOrEmpty(Instructions))
            {
                texts += instrText.GetLocalBounds().Height;
                texts += style.border * 2;
            }

            if (Horizontal)
            {
                return new Vector2f(dimensions.X * entries.Count, dimensions.Y + texts);
            }
            else
            {
                return new Vector2f(dimensions.X, dimensions.Y * entries.Count + texts);
            }
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            if (dirty && Visible)
            {
                Visible = true;
            }
            if (!visible) return;

            if (!string.IsNullOrEmpty(Title))
            {
                target.Draw(titleText);
            }

            if (!string.IsNullOrEmpty(Instructions))
            {
                target.Draw(instrText);
            }

            foreach (var entry in entries)
            {
                target.Draw(entry.shape);
                target.Draw(entry.text);
            }
        }

        public MenuGUI()
        {
            visible = false;

            entries = new List<GUIEntry>();
        }
        public MenuGUI(Vector2f dimensions)
            : this()
        {
            this.dimensions = dimensions;
        }
        public MenuGUI(Vector2f dimensions, List<Tuple<string, string>> entries)
           : this(dimensions)
        {
            var shape = new RectangleShape();
            shape.Size = dimensions;
            shape.FillColor = style.GetColor(0);
            shape.OutlineThickness = -style.border;
            shape.OutlineColor = style.GetColor(1);

            foreach (var entry in entries)
            {
                var text = new Text();
                text.DisplayedString = entry.Item1;
                text.Font = style.bodyFont;
                text.Color = style.GetColor(1);
                text.CharacterSize = style.itemSize;

                this.entries.Add(new GUIEntry(entry.Item2, new RectangleShape(shape), text));
            }

        }
    }
}
