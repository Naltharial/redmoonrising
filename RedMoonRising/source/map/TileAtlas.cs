﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.map
{
    class TileAtlas : IDictionary<string, Tile>
    {
        private IDictionary<string, Tile> typeClass;

        public Tile AtPosition(Vector2f point)
        {
            foreach (var item in typeClass)
            {
                if (item.Value.PointInPolygon(point))
                {
                    return item.Value;
                }
            }

            return null;
        }

        public TileAtlas()
        {
            typeClass = new Dictionary<string, Tile>();
        }

        #region typeClass passthrough
        public Tile this[string key]
        {
            get
            {
                return typeClass[key];
            }

            set
            {
                typeClass[key] = value;
            }
        }

        public int Count
        {
            get
            {
                return typeClass.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return typeClass.IsReadOnly;
            }
        }

        public ICollection<string> Keys
        {
            get
            {
                return typeClass.Keys;
            }
        }

        public ICollection<Tile> Values
        {
            get
            {
                return typeClass.Values;
            }
        }

        public void Add(KeyValuePair<string, Tile> item)
        {
            typeClass.Add(item);
        }

        public void Add(string key, Tile value)
        {
            typeClass.Add(key, value);
        }

        public void Clear()
        {
            typeClass.Clear();
        }

        public bool Contains(KeyValuePair<string, Tile> item)
        {
            return typeClass.Contains(item);
        }

        public bool ContainsKey(string key)
        {
            return typeClass.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<string, Tile>[] array, int arrayIndex)
        {
            typeClass.CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<string, Tile>> GetEnumerator()
        {
            return typeClass.GetEnumerator();
        }

        public bool Remove(KeyValuePair<string, Tile> item)
        {
            return typeClass.Remove(item);
        }

        public bool Remove(string key)
        {
            return typeClass.Remove(key);
        }

        public bool TryGetValue(string key, out Tile value)
        {
            return typeClass.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return typeClass.GetEnumerator();
        }
        #endregion
    }
}
