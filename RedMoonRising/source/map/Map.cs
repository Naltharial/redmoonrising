﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

using SFML.System;
using SFML.Graphics;

namespace RedMoonRising.source.map
{
    class Map : core.Drawn
    {
        public Tile Selected
        {
            get;
            private set;
        }
        public uint size;

        private Sprite actSprite;
        private RectangleShape actShape;

        private Sprite bgSprite;
        private Texture map;
        private Texture zones;
        private TileAtlas tileAtlas;
        
        public bool Select(Tile tile)
        {
            Selected = tile;

            return tile != null;
        }
        public void ClearSelect()
        {
            Selected = null;
        }

        public Tuple<bool, Tile> AtPosition(Vector2f point)
        {
            var tile = tileAtlas.AtPosition(point);

            var detail = false;
            if (tile != null)
            {
                if (tile.CenterDistance(point) < 0.075F * actSprite.Texture.Size.X)
                {
                    detail = true;
                }
            }

            return new Tuple<bool, Tile>(detail, tile);
        }

        public Tile TileNamed(string name)
        {
            return tileAtlas[name];
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            bgSprite.Draw(target, states);

            foreach (var item in tileAtlas)
            {
                if (item.Value != Selected)
                {
                    item.Value.Draw(target, states);

                    actSprite.Position = item.Value.Center;
                    actShape.Origin = -item.Value.Center;

                    actShape.Draw(target, states);
                    actSprite.Draw(target, states);
                }
            }

            if (Selected != null)
            {
                Selected.DrawSelected(target, states);

                actSprite.Position = Selected.Center;
                actShape.Origin = -Selected.Center;

                actShape.Draw(target, states);
                actSprite.Draw(target, states);
            }
        }

        public Map(uint size, Texture map, Texture zones, TileAtlas tileAtlas)
        {
            bgSprite = new Sprite();

            this.size = size;
            this.map = map;
            this.zones = zones;
            this.tileAtlas = tileAtlas;

            bgSprite.Texture = map;

            var tex = new Texture("resources/art/icons/abstract-080.png");
            actSprite = new Sprite(tex);
            actSprite.Scale = new Vector2f(0.08F, 0.08F);
            actSprite.Color = Color.Magenta;
            actSprite.Origin = new Vector2f((actSprite.Texture.Size.X - 20 * style.border) / 2 , (actSprite.Texture.Size.Y - 20 * style.border) / 2);

            actShape = new RectangleShape();
            actShape.Size = new Vector2f(0.08F * actSprite.Texture.Size.X + 2 * style.border, 0.08F * actSprite.Texture.Size.Y + 2 * style.border);
            actShape.Position = new Vector2f(-0.08F * actSprite.Texture.Size.X / 2, -0.08F * actSprite.Texture.Size.Y / 2);
            actShape.FillColor = style.GetColor(0);
            actShape.OutlineThickness = -style.border;
            actShape.OutlineColor = style.GetColor(1);
        }
    }
}
