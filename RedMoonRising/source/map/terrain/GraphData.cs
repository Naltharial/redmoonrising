﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.map.terrain
{
    class GraphData
    {
        public csDelaunay.Site Site
        {
            get;
            set;
        }

        private List<double> heightData;
        private List<double> moistureData;
        private List<double> resourceData;

        public void AddHeight(double point)
        {
            heightData.Add(point);
        }
        public double GetHeight()
        {
            return heightData.Average();
        }

        public void AddMoisture(double point)
        {
            moistureData.Add(point);
        }
        public double GetMoisture()
        {
            return moistureData.Average();
        }

        public void AddResource(double point)
        {
            resourceData.Add(point);
        }
        public double GetResource()
        {
            return resourceData.Average();
        }

        public GraphData()
        {
            heightData = new List<double>();
            moistureData = new List<double>();
            resourceData = new List<double>();
        }
    }
}
