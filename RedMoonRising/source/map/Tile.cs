﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.map
{
    enum TileType
    {
        LAND, GULF
    }

    class Tile : core.Drawn, IDisposable
    {
        public string Id
        {
            get;
            private set;
        }
        public string Name
        {
            get;
            private set;
        }
        public Color Color
        {
            get;
            private set;
        }
        public Vector2f Center
        {
            get;
            private set;
        }

        public actors.Faction Owner
        {
            get;
            set;
        }

        private VertexArray borders;
        private VertexArray baseBorders;
        private VertexArray selectedBorders;
        private List<CircleShape> pointBorders;
        private List<CircleShape> pointSBorders;

        // Raycasting algorithm
        public bool PointInPolygon(Vector2f point)
        {
            uint i, j, nvert = borders.VertexCount;
            bool c = false;

            for (i = 0, j = nvert - 1; i < nvert; j = i++)
            {
                if (((borders[i].Position.Y >= point.Y) != (borders[j].Position.Y >= point.Y)) &&
                        (point.X <= (borders[j].Position.X - borders[i].Position.X) *
                        (point.Y - borders[i].Position.Y) /
                        (borders[j].Position.Y - borders[i].Position.Y) + borders[i].Position.X))
                {
                    c = !c;
                }
            }

            return c;
        }

        public float CenterDistance(Vector2f point)
        {
            var asq = Math.Pow(Center.X - point.X, 2);
            var bsq = Math.Pow(Center.Y - point.Y, 2);

            return (float)Math.Sqrt(asq + bsq);
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            baseBorders.Draw(target, states);

            foreach (var item in pointBorders)
            {
                item.Draw(target, states);
            }
        }
        public void DrawSelected(RenderTarget target, RenderStates states)
        {
            selectedBorders.Draw(target, states);

            foreach (var item in pointSBorders)
            {
                item.Draw(target, states);
            }
        }

        public Tile(string id, string name, Color color, Vector2f center, VertexArray borders)
        {
            Id = id;
            Name = name;
            Color = color;
            Center = center;

            this.borders = borders;

            baseBorders = new VertexArray(PrimitiveType.Quads, (borders.VertexCount + 1) * 4);
            selectedBorders = new VertexArray(PrimitiveType.Quads, (borders.VertexCount + 1) * 4);
            pointBorders = new List<CircleShape>();
            pointSBorders = new List<CircleShape>();
            for (int i = 1; i <= borders.VertexCount; i++)
            {
                var start = borders[(uint)i - 1];
                Vertex end = borders[0];
                if (i < borders.VertexCount)
                {
                    end = borders[(uint)i];
                }

                var direction = end.Position - start.Position;
                var length = (float)Math.Sqrt(direction.X*direction.X + direction.Y*direction.Y);
                var unitd = direction / length;
                var unitperp = new Vector2f(-unitd.Y, unitd.X);
                var offset = unitperp * style.tileBorder;

                var ver1 = new Vertex();
                ver1.Color = style.GetColor(style.tileColor);
                ver1.Position = offset + start.Position;
                var ver2 = new Vertex();
                ver2.Color = style.GetColor(style.tileColor);
                ver2.Position = direction + offset + start.Position;
                var ver3 = new Vertex();
                ver3.Color = style.GetColor(style.tileColor);
                ver3.Position = direction - offset + start.Position;
                var ver4 = new Vertex();
                ver4.Color = style.GetColor(style.tileColor);
                ver4.Position = start.Position - offset;

                var startPoint = (uint)(i * 4);
                baseBorders[startPoint] = ver1;
                baseBorders[startPoint + 1] = ver2;
                baseBorders[startPoint + 2] = ver3;
                baseBorders[startPoint + 3] = ver4;

                var cs1 = new CircleShape();
                cs1.FillColor = style.GetColor(style.tileColor);
                cs1.Radius = style.tileBorder;
                cs1.Position = start.Position - new Vector2f(style.tileBorder, style.tileBorder);
                pointBorders.Add(cs1);

                offset = unitperp * style.tileSBorder;

                var sver1 = new Vertex();
                sver1.Color = color;
                sver1.Position = offset + start.Position;
                var sver2 = new Vertex();
                sver2.Color = color;
                sver2.Position = direction + offset + start.Position;
                var sver3 = new Vertex();
                sver3.Color = color;
                sver3.Position = direction - offset + start.Position;
                var sver4 = new Vertex();
                sver4.Color = color;
                sver4.Position = start.Position - offset;

                selectedBorders[startPoint] = sver1;
                selectedBorders[startPoint + 1] = sver2;
                selectedBorders[startPoint + 2] = sver3;
                selectedBorders[startPoint + 3] = sver4;

                var css1 = new CircleShape();
                css1.FillColor = color;
                css1.Radius = style.tileSBorder;
                css1.Position = start.Position - new Vector2f(style.tileSBorder, style.tileSBorder);
                pointSBorders.Add(css1);
            }
        }
    }
}
