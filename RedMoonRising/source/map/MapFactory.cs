﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace RedMoonRising.source.map
{
    static class MapFactory
    {
        private static Jurassic.ScriptEngine engine;

        private static string mapi = "resources/map/map.png";
        private static string zonesi = "resources/map/zones.png";
        private static string jsf = "resources/scripts/map.js";

        public static Map GenerateFrame()
        {
            var map = new Texture(mapi);
            var zones = new Texture(zonesi);

            var vs = new TileAtlas();

            var mapjs = engine.GetGlobalValue<Jurassic.Library.ObjectInstance>("map");
            
            var size = Convert.ToUInt32(mapjs["size"]);

            int n = 1;
            var mapprovs = (Jurassic.Library.ObjectInstance)mapjs["provinces"];
            foreach (var item in mapprovs.Properties)
            {
                var provid = item.Name;
                var province = (Jurassic.Library.ObjectInstance)item.Value;
                var provname = (string)province["name"];
                var provcolors = (string)province["color"];
                var provcint = int.Parse(provcolors, System.Globalization.NumberStyles.HexNumber);
                System.Drawing.Color argbcolor = System.Drawing.Color.FromArgb(provcint);
                var provcolor = new Color(argbcolor.R, argbcolor.G, argbcolor.B);
                var coords = (Jurassic.Library.ArrayInstance)province["borders"];
                var centerarr = (Jurassic.Library.ArrayInstance)province["center"];
                var center = new Vector2f(Convert.ToSingle(centerarr[0]), Convert.ToSingle(centerarr[1]));

                var vl = new VertexArray(PrimitiveType.LinesStrip);

                foreach (var coord in coords.ElementValues)
                {
                    var ca = (Jurassic.Library.ArrayInstance)coord;
                    var cs = new Vertex(new Vector2f((int)ca[0], (int)ca[1]), provcolor);
                    vl.Append(cs);
                }
                var tile = new Tile(provid, provname, provcolor, center, vl);
                vs.Add(provid, tile);
                n++;
            }

            var mapobj = new Map(size, map, zones, vs);

            return mapobj;
        }

        static MapFactory()
        {
            engine = core.scripting.JEngineFactory.GetJEngine(jsf);
        }
    }
}
