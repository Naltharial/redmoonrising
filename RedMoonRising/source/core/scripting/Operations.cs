﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Jurassic;
using Jurassic.Library;

namespace RedMoonRising.source.core.scripting
{
    class OperationsConstructor : ClrFunction
    {
        public OperationsConstructor(ScriptEngine engine)
            : base(engine.Function.InstancePrototype, "Operations", new OperationsInstance(engine.Object.InstancePrototype))
        {
        }

        [JSConstructorFunction]
        public OperationsInstance Construct()
        {
            return new OperationsInstance(InstancePrototype);
        }
    }

    class OperationsInstance : ObjectInstance
    {
        public OperationsInstance(ObjectInstance prototype)
            : base(prototype)
        {
            PopulateFunctions();
            
            base.DefineProperty("always", new PropertyDescriptor(true, PropertyAttributes.Sealed), true);
            base.DefineProperty("never", new PropertyDescriptor(false, PropertyAttributes.Sealed), true);
        }
    }
}
