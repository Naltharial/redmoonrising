﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Jurassic;
using Jurassic.Library;

namespace RedMoonRising.source.core.scripting
{
    class ValueConstructor : ClrFunction
    {
        public ValueConstructor(ScriptEngine engine, object value)
            : base(engine.Function.InstancePrototype, "Value", new ValueInstance(engine.Object.InstancePrototype, value))
        {
        }

        [JSConstructorFunction]
        public ValueInstance Construct(object value)
        {
            return new ValueInstance(InstancePrototype, value);
        }
    }

    class ValueInstance : ObjectInstance
    {
        private Type objType;

        [JSFunction(Name = "add")]
        public void AddAmount(dynamic add)
        {
            this["value"] += add;
        }
        [JSFunction(Name = "equals")]
        public bool CheckEquality(dynamic eq)
        {
            if (eq.GetType() != objType)
            {
                return false;
            }

            return this["value"] == eq;
        }

        public ValueInstance(ObjectInstance prototype, object value)
            : base(prototype)
        {
            PopulateFunctions();

            objType = value.GetType();
            base.DefineProperty("value", new PropertyDescriptor(value, PropertyAttributes.Sealed), true);
        }
    }
}