﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Jurassic;
using Jurassic.Library;

namespace RedMoonRising.source.core.scripting
{
    class CharacterConstructor : ClrFunction
    {
        public CharacterConstructor(ScriptEngine engine, string id)
            : base(engine.Function.InstancePrototype, "Character", new CharacterInstance(engine.Object.InstancePrototype, id))
        {
        }

        [JSConstructorFunction]
        public CharacterInstance Construct(string id)
        {
            return new CharacterInstance(InstancePrototype, id);
        }
    }

    class CharacterInstance : ObjectInstance
    {
        [JSFunction(Name = "equals")]
        public bool CheckEquality(CharacterInstance eq)
        {
            return ((ValueInstance)this["id"]).CheckEquality((ValueInstance)eq["id"]);
        }

        public static CharacterInstance FromCharacter(ScriptEngine engine, actors.Character character)
        {
            var pi = new CharacterInstance(engine.Object.InstancePrototype, character.Id.ToString());

            return pi;
        }

        public CharacterInstance(ObjectInstance prototype, string id)
            : base(prototype)
        {
            PopulateFunctions();
            
            base.DefineProperty("id", new PropertyDescriptor(new ValueInstance(prototype, id), PropertyAttributes.Sealed), true);
            base.DefineProperty("money", new PropertyDescriptor(new ValueInstance(prototype, 0), PropertyAttributes.Sealed), true);
        }
    }
}
