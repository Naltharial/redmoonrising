﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Jurassic;

namespace RedMoonRising.source.core.scripting
{
    static class JEngineFactory
    {
        #region file constructors
        public static ScriptEngine GetJEngine(params string[] files)
        {
            var engine = GetJEngine();

            foreach (var item in files)
            {
                engine.ExecuteFile(item);
            }

            return engine;
        }
        public static ScriptEngine GetJEngine(List<string> files)
        {
            return GetJEngine(files.ToArray());
        }
        #endregion

        public static ScriptEngine GetJEngine()
        {
            var engine = new ScriptEngine();

            engine.SetGlobalValue("op", new OperationsInstance(engine.Object.InstancePrototype));

            return engine;
        }
    }
}
