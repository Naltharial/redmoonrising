﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Jurassic;
using Jurassic.Library;

namespace RedMoonRising.source.core.scripting
{
    class ProvinceConstructor : ClrFunction
    {
        public ProvinceConstructor(ScriptEngine engine, string name, string chid)
            : base(engine.Function.InstancePrototype, "Province", new ProvinceInstance(engine.Object.InstancePrototype, name, chid))
        {
        }

        [JSConstructorFunction]
        public ProvinceInstance Construct(string name, string chid)
        {
            return new ProvinceInstance(InstancePrototype, name, chid);
        }
    }

    class ProvinceInstance : ObjectInstance
    {
        [JSFunction(Name = "attack")]
        public bool AttackProvince()
        {
            return true;
        }
        [JSFunction(Name = "defend")]
        public bool DefendProvince()
        {
            return true;
        }

        public static ProvinceInstance FromTile(ScriptEngine engine, map.Tile tile)
        {
            var pi = new ProvinceInstance(engine.Object.InstancePrototype, tile.Id, tile.Owner.GetController(tile).Id.ToString());

            return pi;
        }

        public ProvinceInstance(ObjectInstance prototype, string name, string chid)
            : base(prototype)
        {
            PopulateFunctions();

            base.DefineProperty("owner", new PropertyDescriptor(new CharacterInstance(prototype, chid), PropertyAttributes.Sealed), true);
        }
    }
}
