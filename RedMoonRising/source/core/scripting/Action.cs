﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.core.scripting
{
    class Action
    {
        public string Name
        {
            get;
            private set;
        }

        public bool Available
        {
            get;
            private set;
        }

        private Action<map.Tile, actors.Character> action;

        public void Activate(map.Tile tile, actors.Character character)
        {
            action.Invoke(tile, character);
        }

        public Action(string name, bool available, Action<map.Tile, actors.Character> act)
        {
            Name = name;
            Available = available;

            action = act;
        }
    }
}
