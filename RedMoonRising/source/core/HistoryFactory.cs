﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.core
{
    static class HistoryFactory
    {
        private static Jurassic.ScriptEngine engine;
        
        private static string jsr = "resources/scripts/races.js";
        private static string jst = "resources/scripts/traits.js";
        private static string jsp = "resources/scripts/portraits.js";

        public static History LoadFile(string file)
        {
            engine.ExecuteFile(file);
            var historyjs = engine.GetGlobalValue<Jurassic.Library.ObjectInstance>("history");
            var playerjs = (Jurassic.Library.ObjectInstance)historyjs["player"];
            var charactersjs = (Jurassic.Library.ObjectInstance)historyjs["characters"];
            var provincesjs = (Jurassic.Library.ObjectInstance)historyjs["provinces"];
            var factionsjs = (Jurassic.Library.ObjectInstance)historyjs["factions"];

            var pid = (string)playerjs["character"];
            var gid = Guid.Parse(pid);

            // Get game map
            var nmap = map.MapFactory.GenerateFrame();

            var characters = new actors.Characters();
            var factions = new actors.Factions();

            foreach (var item in factionsjs.Properties)
            {
                var fjs = (Jurassic.Library.ObjectInstance)item.Value;
                var guid = Guid.Parse(item.Name);

                var f = new actors.Faction(guid, (string)fjs["name"]);
                factions.Add(f);
            }

            foreach (var item in charactersjs.Properties)
            {
                var cjs = (Jurassic.Library.ObjectInstance)item.Value;
                var rjs = (string)((Jurassic.Library.ObjectInstance)cjs["race"])["name"];
                var guid = Guid.Parse(item.Name);

                actors.Portrait portrait = null;
                if (guid == gid)
                {
                    portrait = actors.CharacterFactory.GetPlayerPortrait();
                }
                else
                {
                    portrait = actors.CharacterFactory.GetRandomPortrait(rjs, (string)cjs["gender"]);
                }
                
                var c = new actors.Character(guid, (string)cjs["name"], (string)cjs["gender"], rjs,
                    Convert.ToDateTime((string)cjs["birth"]), portrait);
                c.Money = Convert.ToUInt32(cjs["money"]);
                characters.Add(c);

                var fid = Guid.Parse((string)cjs["faction"]);
                factions[fid].AddCharacter(c);
            }

            foreach (var item in provincesjs.Properties)
            {
                var pjs = (Jurassic.Library.ObjectInstance)item.Value;
                var cid = Guid.Parse((string)pjs["controller"]);
                var tile = nmap.TileNamed(item.Name);

                var c = characters[cid];
                c.Faction.AddController(c, tile);
            }

            var hist = new History(nmap, gid, characters, factions);
            Debug.WriteLine("[HistoryFactory] Loaded " + file);
            return hist;
        }

        static HistoryFactory()
        {
            engine = scripting.JEngineFactory.GetJEngine(new string[] { jsr, jst, jsp });
        }
    }
}
