﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.core
{
    class History
    {
        public map.Map Map
        {
            get;
            protected set;
        }
        public Guid PlayerId
        {
            get;
            protected set;
        }
        public actors.Characters Characters
        {
            get;
            protected set;
        }
        public actors.Factions Factions
        {
            get;
            protected set;
        }

        public History(map.Map map, Guid playerId, actors.Characters characters, actors.Factions factions)
        {
            Map = map;
            PlayerId = playerId;
            Characters = characters;
            Factions = factions;
        }
    }
}
