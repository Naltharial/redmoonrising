﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.core
{
    class DistributedRandom : Random
    {
        private static double normalMean = 0.0;
        private static double normalDev = 1.0;
        private static double normalCut = 3.5;

        // Normal distribution - PDF
        private static double NormalPDF(double x, double mean, double deviation)
        {
            var ds = Math.Pow(deviation, 2);
            var exp = Math.Pow(x - mean, 2) / 2 * ds;

            return Math.Pow(1 / Math.Sqrt(2 * Math.PI * ds) * Math.E, -exp);
        }
        private static double NormalPDF(double x)
        {
            return NormalPDF(x, normalMean, normalDev);
        }

        // Box-Muller transform
        private double NextNormalRaw()
        {
            double lim1, lim2 = NextDouble();

            do
            {
                lim1 = NextDouble();
            } while (lim1 <= double.MinValue);

            var u1 = Math.Sqrt(-2 * Math.Log(lim1)) * Math.Cos(2 * Math.PI * lim2);
            var u2 = Math.Sqrt(-2 * Math.Log(lim1)) * Math.Sin(2 * Math.PI * lim2);
            return u1;
        }
        
        public int NextNormal(int maxValue)
        {
            var nnr = Math.Abs(NextNormalRaw());
            var mnr = Math.Min(nnr, normalCut);
            return (int)(nnr * maxValue / normalCut);
        }
        public int NextNormal()
        {
            return NextNormal(int.MaxValue);
        }
        public int NextNormal(int minValue, int maxValue)
        {
            if (minValue > maxValue)
            {
                throw new ArgumentOutOfRangeException("minValue is greater than maxValue.");
            }

            var range = maxValue - minValue;
            return NextNormal(range) + minValue;
        }
    }
}
