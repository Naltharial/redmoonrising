﻿using SFML.Graphics;

namespace RedMoonRising.source.core
{
    public abstract class Drawn : Transformable, Drawable
    {
        public static readonly gui.GUIStyle style = new gui.GUIStyle();

        public abstract void Draw(RenderTarget target, RenderStates states);
    }
}
