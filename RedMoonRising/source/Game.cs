﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading;

using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace RedMoonRising.source
{
    using states;
    class Game
    {
        private RenderWindow window;
        private texture.TextureManager textureManager;

        private Stack<State> states;
        private Queue<StateChange> changes;

        private IDictionary<string, Thread> threads;

        private uint fps, cps;

        public void RenderLoop(object window)
        {
            var style = new gui.GUIStyle();
            var state = RenderStates.Default;

            Clock clock = new Clock();

            var w = (RenderWindow)window;

            while (w.IsOpen)
            {
                w.Clear(style.GetColor(5));

                var elapsed = clock.Restart();
                Top()?.UpdateDraw(w, state, elapsed.AsMicroseconds());

                w.Display();

                // Yield and wait
                Thread.Sleep(1);
            }
        }

        public void GameLoop()
        {
            Clock clock = new Clock();

            var mm = new states.menu.MainMenu();
            PushState(mm);

            long offset = (long)((1.0 / cps) * 1000000);
            long leftover = 0;
            while (window.IsOpen)
            {
                window.DispatchEvents();
                HandleChanges();

                // Controlled timestep
                var elapsed = clock.Restart();
                leftover += elapsed.AsMicroseconds();
                while (leftover > offset)
                {
                    Top().UpdateState(offset);

                    leftover -= offset;
                }

                // Yield and wait
                Thread.Sleep(1);
            }
        }

        public void HandleChanges()
        {
            while (changes.Count > 0)
            {
                var sc = changes.Dequeue();

                Debug.WriteLine("[GAME] State change: " + sc.newState);
                if (sc.newState == typeof(ExitState))
                {
                    window.Close();
                }
                else if (sc.newState == typeof(PopState))
                {
                    states.Pop();
                }
                else
                {
                    if (sc.GetType() == typeof(StateSet))
                    {
                        states.Clear();
                    }

                    // IoC injection
                    var param = new List<object>();
                    if (sc.newState.IsSubclassOf(typeof(TextureState)))
                    {
                        param.Add(textureManager);
                    }
                    if (sc.newState == typeof(GameState))
                    {
                        param.Add((Vector2f)window.Size);

                        object history = core.HistoryFactory.LoadFile("resources/history/current.js");
                        param.Add(history);
                    }
                    states.Push((State)Activator.CreateInstance(sc.newState, param.ToArray()));
                }
            }
        }

        public void HandleInput(EventArgs e)
        {
            var topState = Top();
            var paramT = new Type[] { window.GetType(), e.GetType() };
            var hiMethod = topState.GetType().GetMethod("HandleInput", paramT);
            var sc = (StateChange)hiMethod.Invoke(topState, new object[] { window, e });
            if (sc != null)
            {
                changes.Enqueue(sc);
            }
        }

        public State Top()
        {
            if (states.Count > 0)
            {
                return states.First();
            }
            else
            {
                return null;
            }
        }

        public void PushState(State st)
        {
            states.Push(st);
        }
        public void PopState()
        {
            states.Pop();
        }

        public Game(uint fps, uint cps, RenderWindow window)
        {
            this.fps = fps;
            this.cps = cps;
            this.window = window;

            window.SetFramerateLimit(fps);

            textureManager = new texture.TextureManager();

            states = new Stack<State>();
            changes = new Queue<StateChange>();

            threads = new Dictionary<string, Thread>();

            var gui = new Thread(RenderLoop);
            threads.Add("gui", gui);

            gui.Start(window);
        }
    }
}
