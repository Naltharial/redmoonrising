﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.economy
{
    class Resource
    {
        public string Name
        {
            get;
            private set;
        }

        public Resource(string name)
        {
            Name = name;
        }
    }
}
