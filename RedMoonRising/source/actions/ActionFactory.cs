﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedMoonRising.source.actions
{
    static class ActionFactory
    {
        private static Jurassic.ScriptEngine engine;
        
        private static string jsa = "resources/scripts/actions.js";
        private static string jsd = "resources/scripts/decisions.js";

        private static Action<map.Tile, actors.Character> TranslateAction(Jurassic.Library.FunctionInstance jsfunc, Jurassic.Library.ObjectInstance jsobj)
        {
            Action<map.Tile, actors.Character> ra;

            ra = (p, c) => jsfunc.Call(jsobj, core.scripting.ProvinceInstance.FromTile(engine, p), core.scripting.CharacterInstance.FromCharacter(engine, c));

            return ra;
        }

        public static Dictionary<string, core.scripting.Action> GetActions(map.Tile province, actors.Character character)
        {
            var rl = new Dictionary<string, core.scripting.Action>();

            var actions = engine.GetGlobalValue<Jurassic.Library.ObjectInstance>("actions");

            var p = core.scripting.ProvinceInstance.FromTile(engine, province);
            var c = core.scripting.CharacterInstance.FromCharacter(engine, character);

            foreach (var item in actions.Properties)
            {
                var name = item.Name;
                var action = (Jurassic.Library.ObjectInstance)item.Value;

                var act_n = (string)action["name"];
                var act_s = (Jurassic.Library.FunctionInstance)action["show"];
                var act_e = (Jurassic.Library.FunctionInstance)action["enable"];
                var act_a = (Jurassic.Library.FunctionInstance)action["action"];

                var shown = (bool)act_s.Call(action, p, c);
                var enable = (bool)act_e.Call(action, p, c);
                if (shown)
                {
                    var act_inst = new core.scripting.Action(act_n, enable, TranslateAction(act_a, action));
                    rl.Add(name, act_inst);
                }
                
            }

            return rl;
        }

        static ActionFactory()
        {
            engine = core.scripting.JEngineFactory.GetJEngine(jsa, jsd);
        }
    }
}
