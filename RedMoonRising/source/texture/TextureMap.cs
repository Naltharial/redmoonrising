﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;

namespace RedMoonRising.source.texture
{
    class TextureMap : Transformable, Drawable
    {
        private VertexArray vertices;
        private Texture texture;

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= Transform;
            states.Texture = texture;

            target.Draw(vertices, states);
        }

        public TextureMap(uint width, uint height)
        {
            vertices = new VertexArray();
            texture = new Texture(width, height);
        }
    }
}
